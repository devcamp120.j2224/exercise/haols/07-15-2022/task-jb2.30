import com.devcamp.S10.jbr2030.Customer;
import com.devcamp.S10.jbr2030.Invoice;

public class App {
    public static void main(String[] args) throws Exception {
        Customer customer1 = new Customer(8, "Khanh", 10);
        Customer customer2 = new Customer(3, "Loc", 20);
        System.out.println(customer1.toString());
        System.out.println(customer2.toString());

        Invoice invoice1 = new Invoice(23, customer1, 5000);
        Invoice invoice2 = new Invoice(63, customer2, 2000);
        System.out.println("Invoice 1: " + invoice1);
        System.out.println("Invoice 2: " + invoice2);
        System.out.println("Invoice 1 (Discount): " + invoice1.getAmountAfterDiscount());
        System.out.println("Invoice 2 (Discount): " + invoice2.getAmountAfterDiscount());

    }
}
