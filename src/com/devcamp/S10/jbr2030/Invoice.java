package com.devcamp.S10.jbr2030;

public class Invoice {
    private int id;
    private Customer customer;
    private double amount;

    public Invoice() {
        super();
    }

    public Invoice(int id, Customer customer, double amount) {
        this.id = id;
        this.customer = customer;
        this.amount = amount;
    }

    public int getID() {
        return id;
    }

    public void setID(int id) {
        this.id = id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getCustomerID() {
        return customer.getID();
    }

    public String getCustomerName() {
        return customer.getName();
    }

    public int getCustomerDiscount() {
        return customer.getDiscount();
    }

    public double getAmountAfterDiscount() {
        return amount = amount * (100 - getCustomerDiscount()) / 100;
    }

    @Override
    public String toString() {
        return "Invoice [amount=" + amount + ", customer=" + customer + ", id=" + id + "]";
    }

}
